ARG ALPINE_VERSION=3.18
FROM alpine:${ALPINE_VERSION}

ARG KUBE_VERSION=v1.24.15
ARG HELM_VERSION=v3.12.3
ARG SOPS_VERSION=v3.8.0
ARG HELM_SECRETS_VERSION=v4.5.1
ARG HELM_DIFF_VERSION=v3.8.1
ARG HELMFILE_VERSION=0.157.0

RUN apk add --update --no-cache curl git bash gnupg zip && \
    rm -f /var/cache/apk/* && \
    curl -L https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz |tar xvz && \
    mv linux-amd64/helm /usr/bin/helm && \
    chmod +x /usr/bin/helm && \
    rm -rf linux-amd64 && \
    curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_VERSION}/bin/linux/amd64/kubectl -o /usr/bin/kubectl && \
    chmod +x /usr/bin/kubectl && \
    curl -L https://github.com/getsops/sops/releases/download/${SOPS_VERSION}/sops-${SOPS_VERSION}.linux.amd64 -o /usr/local/bin/sops && \
    chmod +x /usr/local/bin/sops && \
    curl -L https://github.com/helmfile/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_${HELMFILE_VERSION}_linux_amd64.tar.gz | tar xvz -C /tmp && \
    mv /tmp/helmfile /usr/local/bin/helmfile && \
    chmod +x /usr/local/bin/helmfile && \
    eval "$(helm env)" && \
    env SKIP_SOPS_INSTALL=true helm plugin install --version "$HELM_SECRETS_VERSION" https://github.com/jkroepke/helm-secrets && \
    rm -rf "$HELM_PLUGINS/helm-secrets/.git" "$HELM_PLUGINS/helm-secrets/tests" && \
    helm plugin install --version "$HELM_DIFF_VERSION" https://github.com/databus23/helm-diff && \
    rm -rf "$HELM_PLUGINS/helm-diff/.git"

WORKDIR /app
CMD ["/bin/bash"]